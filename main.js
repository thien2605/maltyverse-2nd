function main() {
  console.log('Welcome to MALTY VERSE');
  localStorage.setItem('hello', 'Welcome to MALTY VERSE');

  $.fn.multiline = function (text) {
    this.text(text);
    this.html(this.html().replace(/\n/g, '<br/>'));
    return this;
  };

  let tl = gsap.timeline();

  function zoomInBubble(selector) {
    gsap.fromTo(
      selector,
      {
        opacity: 0,
        scale: 0,
        duration: 0.4,
      },
      {
        opacity: 1,
        scale: 1,
        duration: 0.4,
      }
    );
  }

  function zoomOutBubble(selector) {
    gsap.to(selector, {
      opacity: 0,
      scale: 0,
      duration: 0.4,
    });
  }
  /* Anim intro */
  tl.from('.logo-img', { yPercent: 120, duration: 1, clearProps: 'all' }, 0)
    .from('.hero-title-cap', { opacity: 0, duration: 1.5 }, 0)
    .fromTo(
      '.wrapper > span',
      { yPercent: 100 },
      { yPercent: 0, duration: 1, stagger: 0.12, clearProps: 'all' },
      0
    )
    .from(
      '.hero-title-wrapper',
      {
        top: '50%',
        yPercent: -50,
        duration: 0.6,
        clearProps: 'all',
        delay: 0.5,
      },
      1
    )
    .from(
      '.hero-bottom-wrapper',
      {
        yPercent: 100,
        opacity: 0,
        duration: 0.9,
        // delay: -0.24,
        clearProps: 'all',
      },
      '-=0.24'
    )
    .from(
      '.hero-bubble',
      { scale: 0, duration: 0.3, clearProps: 'all', delay: 0.7 },
      2
    )
    .from(
      '.hero-btn-wrapper',
      {
        autoAlpha: 0,
        duration: 0.5,
        'pointer-events': 'none',
        clearProps: 'all',
      },
      2
    );
  /* Anim hover on button */
  let infiniteAnim;
  gsap.set('.hero-img.nope', {
    opacity: 0,
  });
  gsap.set('.hero-img.yes', {
    opacity: 0,
  });
  const handleHoverOutYes = () => {
    gsap.globalTimeline.clear();
    clearTimeout(infiniteAnim);
    $('.ele-item').removeClass('floating');
    $('.ray-group').removeClass('rotating');
    gsap.to('.ele-item', {
      scale: 0,
      opacity: 1,
      duration: 0.4,
      stagger: 0.03,
      ease: 'power3.inOut',
    });
    gsap.to('.ray', {
      opacity: 0,
      duration: 0.2,
    });
    gsap.to('.hero-img.yes', {
      opacity: 0,
      duration: 0.4,
      delay: 0.1,
    });
    gsap.to('.hero-img.nope', {
      opacity: 0,
      duration: 0.4,
      //delay: 0.1,
    });
    // gsap.to('.hero-img.neutral', {
    //   opacity: 1,
    //   duration: 0.4,
    // });
    zoomOutBubble('#yes-bubble');
    zoomOutBubble('#no-bubble');
    zoomInBubble('.hero-bubble.right');
  };
  $('.btn-yes').hover(function () {
    gsap.globalTimeline.clear();
    gsap.to('.ele-item', {
      scale: 1,
      opacity: 1,
      duration: 0.8,
      stagger: 0.03,
      ease: 'power3.inOut',
    });
    gsap.to('.ray', {
      opacity: 1,
      duration: 0.4,
    });
    gsap.to('.hero-img.yes', {
      opacity: 1,
      duration: 0.4,
    });
    // gsap.to('.hero-img.neutral', {
    //   opacity: 0,
    //   duration: 0.4,
    //   delay: 0.1,
    // });
    gsap.set('.hero-img.nope', {
      opacity: 0,
      //duration: 0.4,
      //delay: 0.1,
    });
    zoomInBubble('#yes-bubble');
    zoomOutBubble('#no-bubble');
    zoomOutBubble('.hero-bubble.right');
    infiniteAnim = setTimeout(() => {
      $('.ele-item').addClass('floating');
      $('.ray-group').addClass('rotating');
    }, 800);
  }, handleHoverOutYes);

  $('.btn-no').hover(
    function () {
      gsap.to('.hero-img.nope', {
        opacity: 1,
        duration: 0.4,
      });
      // gsap.to('.hero-img.neutral', {
      //   opacity: 0,
      //   duration: 0.4,
      //   //delay: 0.1,
      // });
      gsap.set('.hero-img.yes', {
        opacity: 0,
        // duration: 0.4,
        // delay: 0.1,
      });
      zoomOutBubble('#yes-bubble');
      zoomInBubble('#no-bubble');
      zoomOutBubble('.hero-bubble.right');
    },
    function () {
      // gsap.to('.hero-img.neutral', {
      //   opacity: 1,
      //   duration: 0.4,
      // });
      gsap.to('.hero-img.nope', {
        opacity: 0,
        duration: 0.4,
        delay: 0.1,
      });
      gsap.to('.hero-img.yes', {
        opacity: 0,
        duration: 0.4,
        delay: 0.1,
      });
      zoomOutBubble('#yes-bubble');
      zoomOutBubble('#no-bubble');
      zoomInBubble('.hero-bubble.right');
    }
  );
  $('.btn-no').on('click', function () {
    window.location.replace('https://www.google.com/');
  });
  // Anim page transition
  const handleClickYes = function () {
    handleHoverOutYes();

    $('.sc-main').addClass('transition');

    const dur =
      parseFloat($('.main-wrapper').css('transition-duration')) * 1000;

    setTimeout(() => {
      $('.main-wrapper').addClass('active-form');
    }, 500);

    setTimeout(() => {
      $('.sc-main').removeClass('transition');
    }, 500 + dur + 200);

    setTimeout(() => {
      $('.form-ele-item').addClass('floating');
    }, 500 + dur + 200 + 500);
  };
  $('.btn-yes').on('click', handleClickYes);

  // Submit form
  $('form').submit(function (e) {
    e.preventDefault();
    console.log(e);
    $('.form-title').text('THANK YOU');
    $('.form-sub-title').text(
      'We are overjoyed to have you with us THE MALTYVERSE AWAIT...'
    );
    $('form').hide();
  });
}

gsap.to('.process-cur', { width: '80%', duration: 0.7, ease: 'easeInOut' });
$(window).on('load', () => {
  setTimeout(() => {
    gsap.to('.process-cur', {
      width: '100%',
      duration: 0.3,
      ease: 'easeInOut',
    });
    $('.loading-wrap, .loading-logo, .process-wrap').addClass('hidden');
    main();
  }, 700);
});
